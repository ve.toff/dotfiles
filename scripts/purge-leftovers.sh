#!/bin/bash

_check_root_permissions() {
	if [ "$EUID" -ne 0 ]; then 
		echo -e "--> \033[33;31merror\033[0m: requires superuser privilege."
	  exit 1
	fi
	true
}

_remove_rc_packages() {
	dpkg --list |grep "^rc" | cut -d " " -f 3 | xargs dpkg --purge
}

_exit() {
	echo -e "Abort"
}

if _check_root_permissions; then 
	echo -n "--> Getting list of removed packages... "
	RC_PACKAGES=($(dpkg --list | grep "^rc" | cut -d " " -f 3 | xargs)); echo "Done"
	
	if [[ ${#RC_PACKAGES[@]} -le 0 ]]; then
		echo "--> No 'rc' packages that can be processed."
		exit 0
	fi
	
	echo -e "--> The following packages will be \e[1mREMOVED\e[0m:\n"
	echo -e "${RC_PACKAGES[@]}\n"
	
	echo -e "--> The configuration files of \e[1m${#RC_PACKAGES[@]}\e[0m removed packages" \
		"are currently present in the system. (it means that the package is not completely removed)\n"

	while true
	do
	    read -r -p '--> Do you want to proceed removing? [y/N] ' choice
	    case "$choice" in
				y|Y) _remove_rc_packages; break;;
				n|N|*) _exit; break;;
	    esac
	done
fi

