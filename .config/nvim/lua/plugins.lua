local status, packer = pcall(require, 'packer')
if (not status) then
	print("[WARNING] Packer is not installed!")
	return
end

vim.cmd [[packadd packer.nvim]]

packer.startup(function(use)
	use 'wbthomason/packer.nvim'
	use 'https://github.com/folke/tokyonight.nvim'
	use 'nvim-lualine/lualine.nvim'
	use 'kyazdani42/nvim-web-devicons'
end)

