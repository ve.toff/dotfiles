# Dotfiles

The repository with my *Dotfiles*.

## *NAVIGATION*

- **SHELL**:
    [**BASH**](https://gitlab.com/muffinsupp1y/dotfiles/-/blob/master/.bashrc),
    [ZSH](https://gitlab.com/ve.toff/dotfiles/-/blob/master/.config/zsh/.zshrc)
- **EDITOR**:
    [**NVIM**](https://gitlab.com/ve.toff/dotfiles/-/tree/master/.config/nvim)
- **TERMINAL**:
    [**ALACRITTY**](https://gitlab.com/muffinsupp1y/dotfiles/-/blob/master/.config/alacritty/alacritty.yml),
    [KITTY](https://gitlab.com/muffinsupp1y/dotfiles/-/blob/master/.config/kitty/kitty.conf),
    [URXVT](https://gitlab.com/muffinsupp1y/dotfiles/-/blob/master/.Xresources)
- **TASKBAR**:
    [TINT2](https://gitlab.com/muffinsupp1y/dotfiles/-/blob/master/.config/tint2/tint2rc),
- **WM**:
    [OPENBOX](https://gitlab.com/muffinsupp1y/dotfiles/-/tree/master/.config/openbox),

## *PREVIEW*
<table align="center">
    <th style="text-align: center" align="center" colspan="2" width="9999">
        XFCE4 SETUP
    </th>
    <tr>
        <td align="center">
            <img src="/images/neofetch-screenshot.png" align="center" alt="neofetch-screenshot.png">
        </td>
        <td align="center">
            <img src="/images/three-windows.png" align="center" alt="three-windows.png">
        </td>
    </tr>
</table>
